# plis.py

[plis.py](https://gitlab.com/carlbordum/plis.py/) is a re-implementation of
[Peter Norvig's legendary lis.py](http://norvig.com/lispy.html) using Python
3.10's new structural pattern matching:
[Specification](https://www.python.org/dev/peps/pep-0634/), [Motivation and
Rationale](https://www.python.org/dev/peps/pep-0635/),
[Tutorial](https://www.python.org/dev/peps/pep-0636/). You should [read the
source](https://gitlab.com/carlbordum/plis.py/-/blob/main/plis.py)!
