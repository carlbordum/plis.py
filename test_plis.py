from plis import tokenize, parse, eval, standard_env

import pytest


@pytest.mark.parametrize("code,expected", [
    ("1", 1),
    ("(+ 4 5)", 9),
    ("(+ 4 5 (+ 6 6) (+ 1 (+ 2 3)))", 27),
    ("(- 3 1)", 2),
    ("(- 3 (+ 1 1))", 1),
    ("(- 15 1 7 1 )", 6),
    ("(* 3 1)", 3),
    ("(* 3 (+ 1 1))", 6),
    ("(* 15 1 2 )", 30),
    ("(/ 16 4)", 4),
    ("(/ 16 4 2)", 2),
    ("(if 1 2 3)", 2),
    ("(if 0 2 3)", 3),
    ("(if (> 5 4) 1 2)", 1),
    ("(if (> 4 5) 1 2)", 2),
    ("(if (< 5 4) 1 2)", 2),
    ("(if (< 4 5) 1 2)", 1),
    ("(if (>= 5 4) 1 2)", 1),
    ("(if (>= 4 5) 1 2)", 2),
    ("(if (>= 5 5) 1 2)", 1),
    ("(if (= 5 5) 1 2)", 1),
    ("(if (= 5 4) 1 2)", 2),
    ("(abs (- 1 5))", 4),
    ("(expt 2 6)", 64),
    ("(max 2 9 6)", 9),
    ("(min 6 9 2)", 2),
    ("(not 1)", 0),
    ("(not 0)", 1),
    ("(length (quote (2 4 6)))", 3),
    ("((lambda (x y) (+ x y y)) 2 3)", 8),
    ("((lambda (x y z) (+ x y z)) 2 3 4)", 9),
    ("(symbol? (quote f))", 1),
    ("(number? 1)", 1),
    ("(null? (quote ()))", 1),
    ("(procedure? (lambda (x) (+ x 2)))", 1),
    ("(map (lambda (x) (* x x)) (list 2 3 4 5))", [4, 9, 16, 25]),
    ("(append (list 2 3) (list 4 5))", [2, 3, 4, 5]),
    ("(car (list 2 3 4))", 2),
    ("(cdr (list 2 3 4))", [3, 4]),
    ("(cons 1 (cons 2 (cons 3 (quote ()))))", [1, 2, 3]),
    ("(apply + (list 1 2 3 4 5))", 15),
])
def test_huh(code, expected):
    env = standard_env()
    result = eval(parse(tokenize(code)), env)
    assert result == expected


def test_define():
    env = standard_env()
    code = "(define x (+ 7 7))"
    eval(parse(tokenize(code)), env)
    result = eval(parse(tokenize("x")), env)
    assert result == 14


def test_norvig_interaction():
    # this is the longest interaction with lis.py in the original post
    def run(code, env):
        return eval(parse(tokenize(code)), env)

    env = standard_env()
    run("(define circle-area (lambda (r) (* pi (* r r))))", env)
    assert round(run("(circle-area 3)", env)) == round(28.274333877)
    run("(define fact (lambda (n) (if (<= n 1) 1 (* n (fact (- n 1))))))", env)
    assert run("(fact 10)", env) == 3628800
    assert run("(fact 100)", env) == 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000
    assert run("(circle-area (fact 10))", env) == 41369087205782.695  # 4.1369087198e+13
    run("(define first car)", env)
    run("(define rest cdr)", env)
    run("(define count (lambda (item L) (if L (+ (equal? item (first L)) (count item (rest L))) 0)))", env)
    assert run("(count 0 (list 0 1 2 3 0 0))", env) == 3
    assert run("(count (quote the) (quote (the more the merrier the bigger the better)))", env) == 4
    run("(define twice (lambda (x) (* 2 x)))", env)
    assert run("(twice 5)", env) == 10
    run("(define repeat (lambda (f) (lambda (x) (f (f x)))))", env)
    assert run("((repeat twice) 10)", env) == 40
    assert run("((repeat (repeat twice)) 10)", env) == 160
    assert run("((repeat (repeat (repeat twice))) 10)", env) == 2560
    assert run("((repeat (repeat (repeat (repeat twice)))) 10)", env) == 655360
    assert run("(pow 2 16)", env) == 65536.0
    run("(define fib (lambda (n) (if (< n 2) 1 (+ (fib (- n 1)) (fib (- n 2))))))", env)
    run("(define range (lambda (a b) (if (= a b) (quote ()) (cons a (range (+ a 1) b)))))", env)
    assert run("(range 0 10)", env) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert run("(map fib (range 0 10))", env) == [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
    assert run("(map fib (range 0 20))", env) == [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765]
